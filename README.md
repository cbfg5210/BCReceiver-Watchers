# BCReceiver-Watchers
[![](https://jitpack.io/v/com.gitee.cbfg5210/BCReceiver-Watchers.svg)](https://jitpack.io/#com.gitee.cbfg5210/BCReceiver-Watchers)


本库作为 [BCReceiver](https://gitee.com/cbfg5210/BCReceiver) 的扩展，将原本在 BCReceiver 中的 watcher 迁移到本项目中，以保持 BCReceiver 的轻量化，本库包含有：
* [BatteryWatcher](https://gitee.com/cbfg5210/BCReceiver-Watchers/blob/master/watcher/src/main/java/cbfg/bcreceiver/watcher/BatteryWatcher.kt)：电池的广播接收器回调
* [HomeWatcher](https://gitee.com/cbfg5210/BCReceiver-Watchers/blob/master/watcher/src/main/java/cbfg/bcreceiver/watcher/HomeWatcher.kt)：home 键的广播接收器回调
* [NetworkWatcher](https://gitee.com/cbfg5210/BCReceiver-Watchers/blob/master/watcher/src/main/java/cbfg/bcreceiver/watcher/NetworkWatcher.kt)：网络的广播接收器回调
* [TimeWatcher](https://gitee.com/cbfg5210/BCReceiver-Watchers/blob/master/watcher/src/main/java/cbfg/bcreceiver/watcher/TimeWatcher.kt)：时间的广播接收器回调

## 引入依赖
### Step 1. Add the JitPack repository to your build file
```gradle
allprojects {
	repositories {
	  ...
	  maven { url 'https://jitpack.io' }
    }
}
```
### Step 2. Add the dependency
```gradle
dependencies {
	implementation 'com.gitee.cbfg5210:BCReceiver-Watchers:$version'
}
```